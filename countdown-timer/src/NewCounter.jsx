import { useState, useEffect } from "react";
import "./App.css";
import hills from "./assets/images/pattern-hills.svg";
import stars from "./assets/images/bg-stars.svg";
import facebook from "./assets/images/icon-facebook.svg";
import pinterest from "./assets/images/icon-pinterest.svg";
import instagram from "./assets/images/icon-instagram.svg";
import "./Counter.css";

function NewCounter(times) {
  // const [deadline, setDeadline] = useState(times);
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  // setDeadline(times);
  const deadline = "July, 18, 2024";

  const getTime = () => {
    const time = Date.parse(deadline) - Date.now();

    setDays(Math.floor(time / (1000 * 60 * 60 * 24)));
    setHours(Math.floor((time / (1000 * 60 * 60)) % 24));
    setMinutes(Math.floor((time / 1000 / 60) % 60));
    setSeconds(Math.floor((time / 1000) % 60));
  };

  useEffect(() => {
    const interval = setInterval(() => getTime(deadline), 1000);

    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <h1>WE'RE LAUNCHING SOON</h1>
      <div className="container">
        <div className="times">
          <div>{days}</div>
          <div>{hours}</div>
          <div>{minutes}</div>
          <div>{seconds}</div>
        </div>
        <div className="ket">
          <div>DAYS</div>
          <div>HOURS</div>
          <div>MINUTES</div>
          <div>SECONDS</div>
        </div>
        {/* <img src={hills}></img> */}

        {/* <img src={stars}></img> */}
      </div>
      <div className="footer">
        <img src={facebook}></img>
        <img src={pinterest}></img>
        <img src={instagram}></img>
      </div>
    </>
  );
}

export default NewCounter;
