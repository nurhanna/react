import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
// import Counter from "./Counter.jsx";
import Countdown from "react-countdown";
import hills from "./assets/images/pattern-hills.svg";

function App() {
  const [count, setCount] = useState(0);
  // Random component
  const Completionist = () => <span>You are good to go!</span>;
  // Renderer callback with condition
  // const renderer = ({ hours, minutes, seconds, completed }) => {
  //   if (completed) {
  //     // Render a completed state
  //     return <Completionist />;
  //   } else {
  //     // Render a countdown
  //     return (
  //       <span>
  //         {hours}:{minutes}:{seconds}
  //       </span>
  //     );
  //   }
  // };

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="container">
        <Countdown date={Date.now() + 5000000} renderer={renderer} />
      </div>
      <img src={hills}></img>
    </>
  );
}

export default App;
