import React from "react";
import ReactDOM from "react-dom/client";
import NewCounter from "./NewCounter.jsx";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./index.css";
const time = "July, 31, 2023";
const router = createBrowserRouter([
  {
    path: "/",
    element: <NewCounter time={time} />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
