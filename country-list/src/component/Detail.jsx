import React from 'react'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import { Link } from 'react-router-dom'
import './Detail.scss'
// mui
import Typography from '@mui/material/Typography'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Button from '@mui/material/Button'
import Skeleton from '@mui/material/Skeleton'
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined'

const Detail = () => {
  let { code } = useParams()
  const [country, setCountry] = useState()
  const [loading, setLoading] = useState(true)
  let url = 'https://restcountries.com/v3.1/alpha/' + code
  useEffect(() => {
    axios
      .get(url)
      .then((response) => {
        setCountry(response.data)
        setLoading(false)
      })
      .catch((e) => {
        console.log(e)
      })
  })
  return (
    <>
      <div className="back">
        <Link to="/">
          <Button variant="outlined">
            <ArrowBackOutlinedIcon />
            &nbsp; Back
          </Button>
        </Link>
      </div>
      {loading ? (
        <div className="container">
          <div className="country-container">
            <div className="flags">
              <Skeleton variant="rectangular" width="100%">
                <div style={{ paddingTop: '57%' }} />
              </Skeleton>
            </div>
            <div className="container-detail">
              <div className="container-text">
                <div className="left">
                  <Skeleton width="100%">
                    <Typography>
                      ...........................................
                    </Typography>
                  </Skeleton>
                  <Skeleton width="100%">
                    <Typography>
                      ...........................................
                    </Typography>
                  </Skeleton>
                  <Skeleton width="100%">
                    <Typography>
                      ...........................................
                    </Typography>
                  </Skeleton>
                  <Skeleton width="100%">
                    <Typography>
                      ...........................................
                    </Typography>
                  </Skeleton>
                </div>
                <div className="right"></div>
              </div>
              <Typography className="border-text"></Typography>
              <div className="border-box">
                <Skeleton width="50%">
                  <Typography>.</Typography>
                </Skeleton>
              </div>
            </div>
          </div>
        </div>
      ) : (
        country?.map((item) => (
          <div className="container">
            <div className="country-container">
              <div className="flags">
                <img src={item.flags.png}></img>
              </div>
              <div className="container-detail">
                <div className="container-text">
                  <div className="left">
                    <Typography variant="h6">
                      <h2>{item.name.common}</h2>
                    </Typography>
                    <Typography>
                      Native Name :
                      {item.name.nativeName
                        ? Object.values(item.name.nativeName)[0].common
                        : ''}
                    </Typography>
                    <Typography>Population : {item.population}</Typography>
                    <Typography>Region : {item.region}</Typography>
                    <Typography>Sub Region : {item.subregion}</Typography>
                    <Typography>Capital : {item.capital}</Typography>
                  </div>
                  <div className="right">
                    <Typography>Top Level Domain : {item.tld}</Typography>
                    <Typography>
                      Currencies :{' '}
                      {item.currencies
                        ? Object.values(item.currencies).map(
                            (currency) => currency.name,
                          )
                        : ''}
                    </Typography>
                    <Typography>
                      Languages :{' '}
                      {item.languages
                        ? Object.values(item.languages).map(
                            (lang, i, listLanguage) => (
                              <span key={i}>
                                {lang}
                                {i < listLanguage.length - 1 && ', '}
                              </span>
                            ),
                          )
                        : ''}
                    </Typography>
                  </div>
                </div>
                <Typography className="border-text">
                  <b>Border Countries </b> :
                </Typography>
                <div className="border-box">
                  {item.borders
                    ? item?.borders?.map((border) => (
                        <div className="border">
                          <Typography>{border}</Typography>
                        </div>
                      ))
                    : 'Nothing'}
                </div>
              </div>
            </div>
          </div>
        ))
      )}
    </>
  )
}
export default Detail
