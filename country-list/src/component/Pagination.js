import React, { useState } from "react";

function usePagination(data, itemPerPage) {
  const [currentPage, setCurrentPage] = useState(1);

  const maxPage = Math.ceil(data?.length / itemPerPage);

  const currentData = () => {
    const begin = (currentPage - 1) * itemPerPage;
    const end = begin + itemPerPage;
    return data?.slice(begin, end);
  };

  const next = () => {
    setCurrentPage((currentPage) => Math.min(currentPage + 1, maxPage));
  };
  const prev = () => {
    setCurrentPage((currentPage) => Math.max(currentPage - 1, 1));
  };

  const jump = (page) => {
    const pageNum = Math.max(1, page);
    setCurrentPage(Math.min(pageNum, maxPage));
  };

  return { next, prev, jump, currentData, currentPage, maxPage };
}
export default usePagination;
