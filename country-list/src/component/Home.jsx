import axios from "axios";
import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./Home.scss";
// mui
import FormControl from "@mui/material/FormControl";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Pagination from "@mui/material/Pagination";
import Button from "@mui/material/Button";
import usePagination from "./Pagination";
import Skeleton from "@mui/material/Skeleton";
import SortByAlphaOutlinedIcon from "@mui/icons-material/SortByAlphaOutlined";

function Home() {
  const [url, setUrl] = useState("https://restcountries.com/v3.1/all/");
  const [country, setCountry] = useState(null);
  const [loading, setLoading] = useState(true);
  const [filter, setFilter] = useState("");
  const [search, setSearch] = useState("");
  const [sort, setSort] = useState("asc");
  const [page, setPage] = useState(1);
  const perPage = 8;
  const count = Math.ceil(country?.length / perPage);
  const countryPagination = usePagination(country, perPage);

  // handle action
  const handleFilter = (event) => {
    setFilter(event.target.value);
    setUrl("https://restcountries.com/v3.1/region/" + event.target.value);
    console.log(url);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
    setUrl("https://restcountries.com/v3.1/name/" + event.target.value);
    console.log("cari");
    console.log(event.target.value);
  };

  const handlePage = (e, p) => {
    setPage(p);
    countryPagination.jump(p);
  };

  function handleSort(e) {
    if (sort == "asc") {
      const strAscending = [...country].sort((a, b) =>
        a.name.common.localeCompare(b.name.common)
      );
      setCountry(strAscending);
    } else if (sort == "desc") {
      const strDes = [...country].sort((a, b) =>
        b.name.common.localeCompare(a.name.common)
      );
      setCountry(strDes);
    }
    setSort(sort == "asc" ? "desc" : "asc");
  }

  useEffect(() => {
    axios
      .get(url)
      .then((response) => {
        setCountry(response?.data);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [url]);
  return (
    <>
      <div className="body-content">
        <div className="search-container">
          <div className="search">
            <TextField
              id="outlined-textarea"
              label=" Search for a country"
              placeholder="Search"
              multiline
              value={search}
              onChange={handleSearch}
              fullWidth
            />
          </div>
          <div className="dropdownButton">
            <FormControl sx={{ mt: 1, minWidth: 150 }}>
              <Select
                value={filter}
                onChange={handleFilter}
                displayEmpty
                inputProps={{ "aria-label": "Without label" }}
              >
                <MenuItem value="">
                  <em>Filter by Region</em>
                </MenuItem>
                <MenuItem value={"Africa"}>Africa</MenuItem>
                <MenuItem value={"America"}>America</MenuItem>
                <MenuItem value={"Asia"}>Asia</MenuItem>
                <MenuItem value={"Europe"}>Europe</MenuItem>
                <MenuItem value={"Oceania"}>Oceania</MenuItem>
              </Select>
            </FormControl>
            <div className="sort">
              <Button
                variant="outlined"
                onClick={() => handleSort()}
                style={{
                  maxHeight: "100%",
                  minHeight: "100%",
                }}
                
              >
                <SortByAlphaOutlinedIcon />
                &nbsp; {sort}
              </Button>
            </div>
          </div>
        </div>
        <div className="country-container">
          {loading
            ? Array.from({ length: 8 }).map((index) => (
                <Link to={"/detail/${item.ccn3}"} key={index}>
                  <div className="country" key={index}>
                    <Card sx={{ maxWidth: 345 }}>
                      <Skeleton animation="wave" height={180} width="90%" />
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                          <Skeleton animation="wave" height={40} width="50%" />
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                          <Skeleton animation="wave" height={30} width="50%" />
                          <Skeleton animation="wave" height={30} width="50%" />
                          <Skeleton animation="wave" height={30} width="50%" />
                        </Typography>
                      </CardContent>
                    </Card>
                  </div>
                </Link>
              ))
            : countryPagination?.currentData().map((item,index) => (
                <Link to={`/detail/${item.ccn3}`} key={index}>
                  <div className="country" key={index}>
                    <Card sx={{  }} className="card">
                      <CardMedia
                        component="img"
                        alt={item.flags.alt}
                        height="140"
                        image={item.flags.png}
                      />
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                          {item.name.common}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                          <p>Population : {item.population}</p>
                          <p>Region : {item.region}</p>
                          <p>Capital : {item.capital}</p>
                        </Typography>
                      </CardContent>
                    </Card>
                  </div>
                </Link>
              ))}
        </div>
      </div>
      {/* pagination */}
      <div className="pagination">
        {loading ? null : (
          <Pagination
            count={count}
            color="primary"
            page={page}
            onChange={handlePage}
          />
        )}
      </div>
    </>
  );
}
export default Home;
