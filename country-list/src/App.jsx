import './App.scss'
import { useState } from 'react'
import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

// theme mui
import { ThemeProvider, createTheme } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import LightModeOutlinedIcon from '@mui/icons-material/LightModeOutlined'
import DarkModeOutlinedIcon from '@mui/icons-material/DarkModeOutlined'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'

// import component
import Home from './component/Home'
import Detail from './component/Detail'

function App() {
  // theme
  const getDesignTokens = (mode = PaletteMode) => ({
    palette: {
      mode,
      ...(mode === 'light'
        ? {
            background: {
              default: 'hsl(0, 0%, 98%)',
            },
            text: {
              primary: 'hsl(200, 15%, 8%)',
            },
            primary: {
              main: 'hsl(200, 15%, 8%)',
            },
          }
        : {
            background: {
              default: 'hsl(207, 26%, 17%)',
            },
            text: {
              primary: 'hsl(0, 0%, 100%)',
            },
            primary: {
              main: 'hsl(0, 0%, 100%)',
            },
          }),
    },
  })
  const [mode, setMode] = useState('light')

  const handleTheme = () => {
    setMode(mode === 'light' ? 'dark' : 'light')
  }
  const theme = React.useMemo(() => createTheme(getDesignTokens(mode)), [mode])

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <>
        <div className="container">
          <div className="header">
            <div className="logo">
              <Typography gutterBottom variant="b" component="div">
                Where in the world?
              </Typography>
            </div>
            <div className="dark-theme">
              <Button size="small" onClick={() => handleTheme()}>
                {mode === 'light' ? (
                  <DarkModeOutlinedIcon />
                ) : (
                  <LightModeOutlinedIcon />
                )}
                &nbsp;
                {mode === 'light' ? 'dark' : 'light'} Mode
              </Button>
            </div>
          </div>
          <div className="body">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/detail/:code" element={<Detail />} />
            </Routes>
          </div>
        </div>
      </>
    </ThemeProvider>
  )
}

export default App
